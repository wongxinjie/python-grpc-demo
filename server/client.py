import sys
import uuid
import socket

import grpc

import topic_pb2
import topic_pb2_grpc


topics = [
    {"user_id": 1, "title": "ZTE fined $1 billion", "content": "After much negotiation with the Trump Administration,"
     "Secretary of Commerce Wilbur Ross confirmed this morning that ZTE,  the Chinese telecommunications giant, has"
     "agreed to a $1 billion fine."},
    {"user_id": 2, "title": "Revolut announces a Robinhood-like trading product", "content": "Fintech startup Revolut"
     "likes to announce new things all the time. Even though nothing is going live today, it’s interesting to see where"
     "the startup is heading. "},
    {"user_id": 3, "title": "Instagram plans to launch Snapchat Discover-style video hub", "content": "Instagram  is"
     "preparing to unveil a home for longer-form video — a YouTube competitor and its take on Snapchat Discover."}
]
SERVER_IP_ADDR = socket.gethostbyname(socket.gethostname())


def log_connectivity_changes(connectivity):
    print("gRPC channel changed status to %s" % connectivity)


channel = grpc.insecure_channel("localhost:50051")
channel.subscribe(log_connectivity_changes, try_to_connect=True)
try:
    grpc.channel_ready_future(channel).result(timeout=10)
except grpc.FutureTimeoutError as err:
    sys.exit("Error connecting to server")
else:
    stub = topic_pb2_grpc.TopicServiceStub(channel)


def public_topic():
    for t in topics:
        topic = topic_pb2.Topic(title=t['title'], content=t['content'])
        request = topic_pb2.PublicTopicRequest(user_id=t['user_id'], topic=topic)
        metadata = (("request-ip", SERVER_IP_ADDR), ('request-id', str(uuid.uuid4())))
        response = stub.publicTopic(request, timeout=2, metadata=metadata)
        print("code => %s, msg => %s" % (response.code, response.msg))


def subscribe_topic(user=0, keyword=''):
    request = topic_pb2.SubscribeTopicRequest(user=user, keyword=keyword)
    metadata = (("request-ip", SERVER_IP_ADDR), ('request-id', str(uuid.uuid4())))
    response = stub.subscribeTopic(request, timeout=2, metadata=metadata)
    print(response.count)
    print(response.topics)


def stream_topic():
    request = topic_pb2.StreamTopicRequest()
    metadata = (("request-ip", SERVER_IP_ADDR), ('request-id', str(uuid.uuid4())))
    response = stub.streamTopic(request, timeout=2, metadata=metadata)
    for r in response:
        print(r)


if __name__ == "__main__":
    public_topic()
    # subscribe_topic()
    stream_topic()
