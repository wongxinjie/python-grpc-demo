import time
import logging
from concurrent import futures

import grpc

import topicdb
import topic_pb2_grpc
import topic_pb2

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s #%(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s')
ONE_DAY_IN_SECONDS = 24 * 60 * 60


class TopicService(topic_pb2_grpc.TopicServiceServicer):

    def publicTopic(self, request, context):
        metadata = dict(context.invocation_metadata())
        logging.debug(metadata)

        code, msg = topicdb.insert(request.user_id, request.topic.title, request.topic.content)
        return topic_pb2.PublicTopicResponse(code=code, msg=msg)

    def subscribeTopic(self, request, context):
        metadata = dict(context.invocation_metadata())
        logging.debug(metadata)

        user = request.user
        keyword = request.keyword
        response = topic_pb2.SubscribeTopicResponse()
        count = 0
        for kw in topicdb.select(user, keyword):
            kw.pop("user_id")
            response.topics.add(**kw)
            count += 1
        response.count = count

        return response

    def streamTopic(self, request, context):
        metadata = dict(context.invocation_metadata())
        logging.debug(metadata)

        for kw in topicdb.select():
            kw.pop("user_id", '')
            topic = topic_pb2.Topic(**kw)
            yield topic_pb2.StreamTopicResponse(topic=topic)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=8))
    topic_pb2_grpc.add_TopicServiceServicer_to_server(TopicService(), server)

    address = "127.0.0.1:50051"
    server.add_insecure_port(address)
    logging.info("running grpc server on %s", address)
    server.start()

    try:
        while True:
            time.sleep(ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt as err:
        server.stop(0)


if __name__ == "__main__":
    serve()
