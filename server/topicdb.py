from datetime import datetime, timezone
topics = []


def insert(user_id, title, content):
    r = {"user_id": user_id, "title": title, "content": content, "created_at": datetime.now(timezone.utc).isoformat()}
    topics.append(r)
    return 200, "succes"


def select(user=None, keyword=None):

    if user and keyword:
        _filter = lambda x: x['user_id'] == user and (keyword in x['title'] or keyword in x['content'])
    elif user:
        _filter = lambda x: x['user_id'] == user
    elif keyword:
        _filter = lambda x: keyword in x['title'] or keyword in x['content']
    else:
        _filter = lambda x: True

    for t in topics:
        if _filter(t):
            yield t
